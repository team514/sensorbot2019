/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


public class SensorUtil extends Subsystem {

  DigitalInput banner0;
  DigitalInput banner1;
  DigitalInput banner2;
  DigitalInput banner3;
  DigitalInput banner4;
  DigitalInput banner5;
  
  public SensorUtil(){
    banner0 = new DigitalInput(0);
    banner1 = new DigitalInput(1);
    banner2 = new DigitalInput(2);
    banner3 = new DigitalInput(3);
    banner4 = new DigitalInput(4);
    banner5 = new DigitalInput(5);  
  }

  public boolean getBanner0() {
    return banner0.get();
  }

  public boolean getBanner1(){
    return banner1.get();
  }

  public boolean getBanner2() {
    return banner2.get();
  }

  public boolean getBanner3(){
    return banner3.get();
  }
  public boolean getBanner4() {
    return banner4.get();
  }

  public boolean getBanner5(){
    return banner5.get();
  }

  @Override
  public void initDefaultCommand() {

  }
  
  public void updateStatus(){
    SmartDashboard.putBoolean("Banner 0 = ", getBanner0());
    SmartDashboard.putBoolean("Banner 1 = ", getBanner1());
    SmartDashboard.putBoolean("Banner 2 = ", getBanner2());
    SmartDashboard.putBoolean("Banner 3 = ", getBanner3());
    SmartDashboard.putBoolean("Banner 4 = ", getBanner4());
    SmartDashboard.putBoolean("Banner 5 = ", getBanner5());
  }

}
