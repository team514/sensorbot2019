/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.DriveMecanum;

/**
 * Add your docs here.
 */
public class DriveUtil extends Subsystem {
  SpeedController leftFront, leftRear, rightFront, rightRear;
  MecanumDrive drive;
  ADXRS450_Gyro gyro;
  Servo servo;
  int pov;

  public boolean arcade = false;

  AnalogInput sonar;
  // DigitalInput banner;

  public DriveUtil(){
    leftFront = new WPI_VictorSPX(RobotMap.leftFrontSC);
    leftRear = new WPI_VictorSPX(RobotMap.leftRearSC);
    rightFront = new WPI_VictorSPX(RobotMap.rightFrontSC);
    rightRear = new WPI_VictorSPX(RobotMap.rightRearSC);

    leftFront.setInverted(true);
    rightFront.setInverted(true);
    leftRear.setInverted(true);
    rightRear.setInverted(true);

    // banner = new DigitalInput(3);
    drive = new MecanumDrive(leftFront, leftRear, rightFront, rightRear);
    drive.setSafetyEnabled(true);
    
    
    gyro = new ADXRS450_Gyro(Port.kOnboardCS0);
  
    servo = new Servo(6);

    sonar = new AnalogInput(0);
    sonar.setAverageBits(1);
    sonar.setOversampleBits(1);
  }

  // public boolean getBannerSensor(){
    // return banner.get();
  // }

  public double getServoPosition(){
    return servo.getAngle();
  }

  public void setServoPosition(double p){
    servo.setAngle(p);
  }

  public void resetGyro(){
    gyro.reset();
  }
  
  public void calibrateGyro(){
    gyro.calibrate();
  }

  public double getGyro(){
    return gyro.getAngle();
  }

  public void driveMecanum(double x, double y, double z){
    x = squaredInputs(x);
    y = squaredInputs(y);
    z = squaredInputs(z);

    drive.driveCartesian(-y, x, -z);
  }

  public double squaredInputs(double d){
    boolean negative = d<0;
    d = Math.pow(d, 2);

    if(negative){
      d*= -1;
    }
    
    return d;
  }
  
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new DriveMecanum());
  }

  public int getPOV(){
    return pov;
  }

  public void setPOV(int pov){
    this.pov = pov;
  }

  public double getDistance(){
    double av = sonar.getAverageVoltage();
    double mm = (1024 * av);
    double inches = (mm / 25.4);
    return inches;
  }

  public void updateStatus(){
    SmartDashboard.putNumber("servo position = ", getServoPosition());
    SmartDashboard.putNumber("dpad value = ", getPOV());
    SmartDashboard.putNumber("sonar distance = ", getDistance());
    SmartDashboard.putNumber("sonar voltage = ", sonar.getAverageVoltage());
    SmartDashboard.putNumber("sonar raw voltage = ", sonar.getVoltage());
    SmartDashboard.putBoolean("arcade = ", arcade);
    // SmartDashboard.putBoolean("banner sensor: ", getBannerSensor());
    // SmartDashboard.putBoolean("banner get method: ", banner.get());
  }
}
